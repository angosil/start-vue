# Start Vue

Start Vue is a simple project in wish I try to put every necessary to start a single page vue project. I start with
Docker to do that I prepare a Dockerfile and docker compose and star with some file structure. This structure is likely
to change throughout development.

## Initial structure

1. `app` The application content. this directory contains the necessary file to create our application.
2. `app/Dockerfile` The initial Dockerfile to star using node and vue.
3. `script` Some important script to facilitate the development of the application.
4. `script/hard_clear.sh` stop and remove all container, images and volumes in local.
5. `script/restart.dev.sh` down, start, and exec bash the docker using docker-compose.
6. `docker-compose.yaml` the start docker-compose file for the application.

## start with vue.

I follow the Vue official documentation, this first step is in
the [Quick Start](https://vuejs.org/guide/quick-start.html) page.

- to star run the script `script/restart.dev.sh` to up and enter to the container.

```bash
$ ./scripts/restart.dev.sh
...
root@7789c8581467:~/app#
```

inside the running docker launch the commands to create te app with the **build tool**.

```bash
root@7789c8581467:~/app# npm init vue@latest
Need to install the following packages:
  create-vue@latest
Ok to proceed? (y)

Vue.js - The Progressive JavaScript Framework

✔ Project name: … project
✔ Add TypeScript? … No / Yes
✔ Add JSX Support? … No / Yes
✔ Add Vue Router for Single Page Application development? … No / Yes
✔ Add Pinia for state management? … No / Yes
✔ Add Vitest for Unit Testing? … No / Yes
✔ Add Cypress for End-to-End testing? … No / Yes
✔ Add ESLint for code quality? … No / Yes
✔ Add Prettier for code formatting? … No / Yes

Scaffolding project in /home/node/app/project...

Done. Now run:

  cd project
  npm install
  npm run lint
  npm run dev

npm notice
npm notice New minor version of npm available! 8.5.5 -> 8.10.0
npm notice Changelog: https://github.com/npm/cli/releases/tag/v8.10.0
npm notice Run npm install -g npm@8.10.0 to update!
npm notice
```

This action create a `project` directory with all necessary files. Go to the `project` directory to install
dependencies and run the dev server.

```bash
root@7789c8581467:~/app# cd project/
root@7789c8581467:~/app/project# npm install

added 490 packages, and audited 491 packages in 11m

67 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```

this generata a lot of files we can find this file in our local project directory in `<project-path>/app/project`.
Because I star the vue development in a running container I need to add some configuration to the file `vite.config.ts`
this file is inside new `<project-path>/app/project` directory.

The original auto generate file is

```ts
import {fileURLToPath, URL} from 'url'

import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), vueJsx()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})

```

I add the server configuration to the `defineConfig` object, and finally I have.

```ts
import {fileURLToPath, URL} from 'url'

import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(), vueJsx()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  //this is new server config.
  server: {
    host: '0.0.0.0',
    watch: {
      usePolling: true
    }
  }
})
```

and now we can run the command

```bash
root@7789c8581467:~/app/project# npm run dev

> project@0.0.0 dev
> vite


  vite v2.9.9 dev server running at:

  > Local:    http://localhost:3000/
  > Network:  http://172.27.0.2:3000/

  ready in 8598ms.

```

Go to [http://localhost:3000/](http://localhost:3000/) and voilá we have our dev environment running.

![vue dev page](doc-images/first.page.png)

and at this point i make a git commit to save all in the repo.

```bash
$ git add .
$ git commit -m "added vue and project"
...
$ git push origin main
```
